# **Overview of Code**
-----------------------

This folder is broken up into two part, �Final Version� and �Old Versions�.  The �Final Version� folder
contains 6 files:

*  **Individual_Messages_20170609.slx:** This is the main simulink file.  
*  **long_train_seq.m:**  	This file is required to be in the working directory.  It creates the long training sequence used for synchronization.
*  **short_train_seq.m:**	This file is required to be in the working directory.  It creates the short training sequence used for synchronization.
*  **lts.mat:**		This file is required to be in the working directory.  This is a matlab .mat file containing time domain complex values of the long training sequence. 
                        This .mat file was generated using the long_train_seq.m file above.
*  **sts.mat:**  		This file is required to be in the working directory.  This is a matlab .mat file containing time domain complex values of the short training sequence.  
                        This .mat file was generated using the short_train_seq.m file above.
*  **Read Me.txt**		This file describes what is in the main file and how to run it.

The �Old Versions� folder is broken up into two folders, �Pilot_Directed_Model_Path� and �Preamble_Directed_Model_Path�.  
The �Pilot_Directed_Model_Path� folder contains many folders that are named with numbers. There are several files inside each folder. 
Each file is a separate design roughly developed around the same time as other files in that folder. 
Many times the name of the file indicates what we were trying to achieve or what significant progress we were able to make in that particular version.  
The higher the number on the folder name, the more recent the collection of files inside that folder. 
So a folder named "05" will have more recent versions than say "04".  
The �Preamble_Directed_Model_Path� folder contains the same file and folder structure as the �Pilot_Directed_Model_Path� folder.  
