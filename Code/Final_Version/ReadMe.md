Read Me

What is in the model?

The final model has two blocks, a Base Station and a Client Station.  The Base Station has three blocks, a Transmit Enable Block, a Transmission Block, and a Reception Block.  The Transmit Enable Block tells the Transmission Block when to transmit.  The Reception Block is always on, and connects the the receive side of the radio.  The client station block has the three same blocks within it, except it has one extra block to control what client number the station will be.

How it works?

The Base Station sends messages to each of the Client Stations in a �round robin� fashion.  It first sends out the message �subcarrier 1�, then �subcarrier 2�, then �subcarrier 3�, then starts over continuously.  Because each Client station is associated with a Client Number, that particular client is looking for its respective message.  Once the client receives it message, then it sends a �Message Received� signal to its Transmit Enable Block.  The Transmit Enable Block takes the first �Message Received� signal to  enable the client's Transmission Block for a brief period and ignores the rest of the �Message Received� signals.  The reason the Transmit Enable Block ignores the rest of the �Message Received� signals is so that it only enables the Transmission Block to once, rather than many times.  Once the Transmission Block is enabled, it sends out a series of �Client# Good� messages back to the Base Station.  Then the process is repeated for each client.

What you need to do to make it work?

Prerequisites:

Computer
Zedboard
Analog Devices FMCOMMS4 Daughter Card
2 Antennas
Ethernet Cable

On Computer:
Install Matlab
Install Simulink
Install Embedded Coder Support Package for Xilinx Zynq-7000 Platform 
Install Embedded Coder Support Package for ARM Cortex-A Processors 
Install HDL Coder Support Package for Xilinx Zynq-7000 Platform 
Install Communications System Toolbox Support Package for Xilinx Zynq-Based Radio 
Install Communications System Toolbox Support Package for Xilinx FPGA-Based Radio 
Install Communications System Toolbox 
Calibrate the carrier offset between each of your clients to the base.  (Use �zynqRadioFrequencyCalibrationTxAD9361AD9364SL� and �zynqRadioFrequencyCalibrationRxAD9361AD9364SL� to do this)

Loop Back Procedure (1 Zedboard):

-Download and place the latest model code, sts.mat, short_train_seq.m, long_train_seq.m, and lts.mat in your working directory.

-Go to the �Model Properties�, then �Callbacks�, then �Initfcn� and scroll down to the bottom.  Change the value of �client_offset� to zero.  (This is because there is no carrier offset when using the same radio).

-In the Base Station Block, comment out the Base Reception Block.

-In the Client Station Block, comment out the Client Transmission Block.

-In the Client Station Block, change the �Client Number� to your desired client station number (1,2,or 3).

-Connect the antennas to Tx1 and Rx1 of the FMCOMMS4 daughter card.

-Play the model.

You should be seeing messages scrolling on the command windows.


Multiple Radio Procedure (2-4 Zedboards):

-Download and place the latest model code, sts.mat, short_train_seq.m, long_train_seq.m, and lts.mat in your working directory.

Base Station Setup:

-Comment out the Client Block.

Client Station Setup:

-Comment out the Base Block.

-On each Client, go to the �Model Properties�, then �Callbacks�, then �Initfcn� and scroll down to the bottom.  Change the value of �client_offset� to the respective carrier offset.  (This is because there is a different carrier offset for every radio with relation to the Base Station).

-In the Client Station Block, change the �Client Number� to your desired client station number (1,2,or 3).  Only one radio can be designated a particular �Client Number� at one time, otherwise they will be interfering with each other and the Base Station will not receive anything from either of them.

-Connect the antennas to Tx1 and Rx1 of the FMCOMMS4 daughter card.

-Play the model on the Base Station and on each Client Station.

You should be seeing messages scrolling on the command windows of both the Client Station and on the Base Station.

