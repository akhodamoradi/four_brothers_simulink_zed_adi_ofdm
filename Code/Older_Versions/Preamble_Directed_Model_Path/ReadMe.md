Folders at this level are named with numbers. There are several files inside each folder. Each file is a separate design roughly developed
around the same time as other files in that folder. Many times the name of the file indicates what we were trying to achieve or what significant
progress we were able to make in that particular version.

The higher the number on the folder name, the more recent the collection of files inside that folder. So a folder named "05" will have more recent versions
than say "04

Version information is only provided for the files inside the 07 folder and can be found in the file 07_Version_Info.txt
Versions prior to V11 can be found in the smaller numbered folders.


 