V1 to V10: Tried several variations to get the spectrum and constellation across. These files are spread across folders 02 to 05.
V11: Implemented our own Frame Sync block.  Start to get some resemblence of the constellation. Far from perfect!
V13: Got the sim to work. But had problems with radio. In the prior versions of this branch, the data being sent over was not OFDM. This version onwards, instead of sending some fixed data, 
     sending over OFDM data. 
     
V14: First attempt at adding radio. No loopback on the same HW or any Radio to Radio communication works. No constellation gets accross.
V15: Another attempt to get the loopback working.
V16: Another attempt. Eventually discarded V15 and V16

V17: Start with the V13 as baseline again and add radio. 

V17_Using_V13_As_Baseline_Adding_Radio_Frame_Works: Overcame one of the radio related problems by attenuating the data path more than
     the preamble. The problem was related to the preamble being too low vs the data signals and hence the XCORR was not able to detect it. This made the loopback work.


V21: Using Frame_Offset_Remove_Sim_Corrected_V17_CarrierTestHW as baseline 
     which Brian had sent over on 5/13/17.
     This version from the TX side, sends 2 frames at a time so that the RX can look at 2 frames so that it has enough data to shift by, after the preamble has been
     detected. This version also has the attenuation on data line, long and short preamble, squelch logic where data is passed for decode only
     if it appears to be with correct preamble and ofcourse this version also has the algorithm to detect preamble. 
     I am calling Frame_Offset_Remove_Sim_Corrected_V17_CarrierTestHW from Brian to be V21_Frame_Offset_Remove_Radio.


V24: More minor modifications to get the constellations thru. Starting to see the constellations, specially in the loopback mode.
V25: Using V24 as baseline.
V26: Brians attempt to integrate his branch with mine. Mainly the difference is that his has 2 extrea XCORR blocks to detect the frame. This somehjow helps in Carrier Freq. Alignment.
V29: Brian's attempt to add channel estimation as per pre lab 5
V36: A major version which works. Will use this as baseline to now use individual sub carrier message scheme. oth message schemes progressing in parallel
V42: Another major version which works. This ties a lot of loose ends. This will serve as the baseline now on. 
V46: Solid constellations and getting the message accross. This versions is non-individual sub carriers message scheme version where one way works very well 
     on loopback and radio to radio. This is the final tested version of non-individual sub carrier message scheme.
     Works very good with continuous constellation, fast speed, good phase offset correction, frame locking most of the time. Does need Radios to calibrate with each other initially to remove any carrier offsets. 
     We used the design that comes with Matlab/Simulink/Zed board radio, to calibrate.
     Hence forth switching over to individual sub carrier scheme, to implement the 2-way communication
V47: Using V46 as baseline and attempting to add the state machine for 2-way comm. Not tested!
V50: 2-way communication implemented. Works decently good.Not final yet!